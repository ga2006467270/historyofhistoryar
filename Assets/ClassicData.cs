﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ClassicsData : MonoBehaviour
{
    Classics mClassic = null;
    [SerializeField] Image mCurrentImg;
    [SerializeField] Sprite mClassicsImg;
    [SerializeField] Sprite mClassicsNullImg;
    public void OpenTreasure()
    {
        if (mClassic != null)
        {
            PlayerMode.Instance.OpenClassicsBox(mClassic.CImg, mClassic.CName);
        }
        else
        {
            Debug.Log("沒有典籍資料");
        }
    }

    public void SetTreasue(Sprite img, string name)
    {
        if (img == null)
        {
            mCurrentImg.sprite = mClassicsNullImg;
            return;
        }
        else
        {
            mCurrentImg.sprite = mClassicsImg;
        }
        mClassic.CImg = img;
        mClassic.CName = name;
    }
}

public class Classics
{
    public Sprite CImg;
    public string CName;
    public string CDescription;
}
