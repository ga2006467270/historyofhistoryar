﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonData : MonoBehaviour
{
    int mMissionNumber;
    string mMissionName;
    int IsDone;
    bool IsDo;
    [SerializeField] Text mMissionNameText;
    [SerializeField] Image[] mStar = new Image[3];
    [SerializeField] Sprite mStarImage;
    [SerializeField] Image mIsCompleteImage;
    [SerializeField] Sprite[] mCompleteImage;

    public void LoadMission()
    {
        Debug.Log("讀取任務 : " + mMissionNumber);
        GameManage.Instance.UseLoadingBar();
        //MissionManager.Instance.LoadMission(mMissionNumber, mMissionName, IsDone != 1 ? true : false, IsDo);
        PlayerMode.Instance.ClearGameCanvasData();
        PlayerMode.Instance.CloseGameCanvas();
    }

    // IsDone : 0 new 1 doing 2 done
    public void SetMission(string missionName, int number, int star, int isDone, bool isDo)
    {
        for (int i = 0; i < star; i++)
        {
            mStar[i].sprite = mStarImage;
        }
        mMissionName = missionName;
        mMissionNameText.text = mMissionName;
        mMissionNumber = number;
        mIsCompleteImage.sprite = mCompleteImage[isDone];
        IsDone = isDone;
        IsDo = isDo;
    }
}
