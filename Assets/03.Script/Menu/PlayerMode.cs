﻿using easyar;
using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// PlayerID
// PHPURL
// SchoolID
// 根據讀取到的長度調整Content

public class PlayerMode : MonoBehaviour
{

    private static PlayerMode mInstance;

    JSONNode mCardResult;
    int mCardUrlCount = 0;
    int mCardNameCount = 0;
    public string CardValue
    {
        get
        {
            if (mCardResult != null)
            {
                mCardUrlCount += 1;
                return mCardResult["output"][mCardUrlCount - 1][1].Value;
            }
            else
            {
                Debug.LogError("mCardResult == null");
                return null;
            }
        }
    }

    public string CardName
    {
        get
        {
            if (mCardResult != null)
            {
                mCardNameCount += 1;
                return mCardResult["output"][mCardNameCount - 1][2].Value;
            }
            else
            {
                Debug.LogError("mCardResult == null");
                return null;
            }
        }
    }

    int mCardDownloadCount = 0;
    public bool CheckLastCard
    {
        get
        {
            mCardDownloadCount += 1;
            if (mCardDownloadCount == (mCardResult["output"].Count))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    bool mRollectionComplete = false;
    bool mRankingComplete = false;
    bool mLoadPlayerDataComplete = false;
    bool mTreasureComplete = false;

    JSONNode mRankResult;
    int mRankCount = 0;
    public JSONNode RankValue
    {
        get
        {
            if (mRankResult != null)
            {
                mRankCount += 1;
                return mRankResult["coinList"][mRankCount - 1];
            }
            else
            {
                Debug.LogError("mRankResult == null");
                return null;
            }
        }
    }
    public GameObject GameCanvasPanel;

    [Space]
    [SerializeField] ImageTrackerBehaviour mTracker;
    [SerializeField] GameObject mEasyARPanel;
    [SerializeField] GameObject mEasyAR;
    [SerializeField] GameObject mMissionPanel;
    [SerializeField] GameObject mTreasurePanel;
    [SerializeField] GameObject mClassicPanel;
    [SerializeField] GameObject mRankPanel;
    [SerializeField] GameObject MapPanel;
    [SerializeField] GameObject FoundNewItemPanel;
    [Space]
    //[SerializeField] GameObject Achievement;

    [Space]
    [SerializeField] GameObject mMissionButton;
    [Space]
    [SerializeField] RectTransform mTreasureionContent;
    [SerializeField] GameObject mTreasureBox;
    [SerializeField] UnityEngine.UI.Image mTreasureBoxSprite;
    [SerializeField] Text mTreasureBoxName;
    [Space]
    [SerializeField] GameObject mBookHintPanel;
    [Space]
    [SerializeField] RectTransform mRankContent;
    [SerializeField] GameObject mPlayerRankPanel;
    [SerializeField] GameObject EnumImage;
    [Space]
    [SerializeField] Text Money;
    [SerializeField] Text Name;

    public static PlayerMode Instance
    {
        get
        {
            return mInstance;
        }
    }

    public bool mClassicComplete
    {
        get
        {
            return mRollectionComplete;
        }

        set
        {
            mRollectionComplete = value;
        }
    }

    public bool MRankingComplete
    {
        get
        {
            return mRankingComplete;
        }

        set
        {
            mRankingComplete = value;
        }
    }

    public bool LoadPlayerDataComplete
    {
        get
        {
            return mLoadPlayerDataComplete;
        }

        set
        {
            mLoadPlayerDataComplete = value;
        }
    }

    public int RankCount
    {
        get
        {
            return mRankCount;
        }

        set
        {
            mRankCount = value;
        }
    }

    private void Awake()
    {
        mInstance = this;
        mEasyAR.SetActive(false);
    }

    public void LoadData(int userID)
    {
        //StartCoroutine(LoadClassics()); // 待修正
        //StartCoroutine(LoadTreasure());
        //StartCoroutine(loadMissionList());
        //StartCoroutine(loadRanking());
        //StartCoroutine(CheckLoading());
        GameCanvasPanel.SetActive(true);
        GameManage.Instance.LoadComplete();
        mMissionPanel.SetActive(true);
        if (GameManage.Instance.ThePlayerData != null)
        { 
            Name.text = GameManage.Instance.ThePlayerData.Name;
        }
    }

    public void SettingBtn()
    {
        Debug.Log("Setting");
    }

    public void BackToLoginButton()
    {
        Debug.Log("回到login");
        LoginUI.Instance.SetLoginUI(true);
        GameCanvasPanel.SetActive(false);
    }

    public void OpenClassicsPanel()
    {
        Debug.Log("打開classicsPanel");
        mClassicPanel.SetActive(true);
        mMissionPanel.SetActive(false);
    }

    public void CloseClassicsPanel()
    {
        Debug.Log("離開classicsPanel");
        mClassicPanel.SetActive(false);
        mMissionPanel.SetActive(true);
    }

    public void OpenClassicsBox(Sprite cImg, string cName)
    {
        mBookHintPanel.SetActive(true);
    }

    public void CloseClassicsBox()
    {
        mBookHintPanel.SetActive(false);
    }

    public void SearchClassics()
    {
        Debug.Log("搜尋祕寶");
    }

    public void OpenRankPanel()
    {
        Debug.Log("打開RankPanel");
        mRankPanel.SetActive(true);
        mMissionPanel.SetActive(false);
    }

    public void CloseRankPanel()
    {
        Debug.Log("離開classicsPanel");
        //OpenPoint.SetActive(true);
        //Achievement.SetActive(false);
        mRankPanel.SetActive(false);
        mMissionPanel.SetActive(true);
    }

    public void OpenMapBtn()
    {
        Debug.Log("開啟Map");
        mEasyAR.SetActive(true);
        mEasyARPanel.SetActive(true);
        GameCanvasPanel.SetActive(false);
        mTracker.StartTracker();
    }

    public void CloseMapBtn()
    {
        mTracker.StopTracker();
        Debug.Log("離開Map");

        mEasyAR.SetActive(false);
        mEasyARPanel.SetActive(false);
        FoundNewItemPanel.SetActive(false);
        GameCanvasPanel.SetActive(true);
    }

    public void OpenQuestionBtn()
    {
        Debug.Log("開啟Question");
    }

    public void CloseQuestionBtn()
    {
        Debug.Log("離開Question");
    }

    public void CloseEasyARPanelButton() 
    {
        FoundNewItemPanel.SetActive(false);
        Debug.Log("CloseEasyARPanel");
    }

    public void OpenEasyARPanel()
    {
        FoundNewItemPanel.SetActive(true);
        Debug.Log("CloseEasyARPanel");
    }

    public void OpenItemData()
    {
        Debug.Log("打開物品資料");
    }

    public void OpenPointBtn()
    {
        Debug.Log("開取Point積分");
        //OpenPoint.SetActive(true);
        //Achievement.SetActive(false);
    }

    public void OpenAchievementBtn()
    {
        Debug.Log("開取Achievement積分");
        //OpenPoint.SetActive(false);
        //Achievement.SetActive(true);
    }

    public void OpenTreasurePanel()
    {
        Debug.Log("打開classicsPanel");
        mTreasurePanel.SetActive(true);
        mMissionPanel.SetActive(false);
    }

    public void CloseTreasurePanel()
    {
        Debug.Log("離開classicsPanel");
        mTreasurePanel.SetActive(false);
        mMissionPanel.SetActive(true);
    }

    public void OpenTreasureBox(Sprite img, string name)
    {
        mTreasureBox.SetActive(true);
        mTreasureBoxSprite.sprite = img;
        mTreasureBoxName.text = name;
    }

    public void CloseTreasureBox()
    {
        mTreasureBox.SetActive(false);
    }

    public void CheckPlayerCoins()
    {
        Money.text = GameManage.Instance.ThePlayerData.Coins.ToString();
    }

    public void CheckPlayerName()
    {
        Name.text = GameManage.Instance.ThePlayerData.Name.ToString();
    }

    public void ActiveLogout()
    {
        Debug.Log("Logout");
    }

    public void Logout()
    {
        Debug.Log("Back to LoginCanvas");
        Debug.Log("清除Ranking 清除 Collection 清除Mission 關閉UI 清除bool");
        ClearGameCanvasData();
        GameManage.Instance.ThePlayerData.Clear();
        LoginUI.Instance.SetLoginUI(true);
    }

    public void ClearGameCanvasData()
    {
        // 刪除missionbtn
        foreach (ButtonData btndata in mMissionPanel.GetComponentsInChildren<ButtonData>())
        {
            Destroy(btndata.gameObject);
        }
        // 刪除Rankingbtn
        foreach (LoadRank rankdata in mRankContent.GetComponentsInChildren<LoadRank>())
        {
            Destroy(rankdata.gameObject);
        }
        // 刪除treasure
        //foreach (DownloadImage Imgdata in mCollectionContent.GetComponentsInChildren<DownloadImage>())
        //{
        //    Destroy(Imgdata.gameObject);
        //}
        CloseGameCanvas();
        mCardNameCount = 0;
        mCardUrlCount = 0;
        mRankCount = 0;
        mCardResult = null;
        mRankResult = null;
        MRankingComplete = false;
        mClassicComplete = false;
        mCardDownloadCount = 0;
        //mTreasureCount = 0;
    }


    public void LoadMissionFromCache(JSONNode node)
    {
        for (int i = 0; i < node["do"].Count; i++)
        {
            GameObject missionbutton = Instantiate(mMissionButton, mMissionPanel.transform);
            ButtonData data = missionbutton.GetComponent<ButtonData>();
            data.SetMission(node["do"][i][1].Value, node["do"][i][0].AsInt, node["do"][i][3].AsInt, node["do"][i][4].AsInt,true);
        }
        for (int i = 0; i < node["yet_do"].Count; i++)
        {
            GameObject missionbutton = Instantiate(mMissionButton, mMissionPanel.transform);
            ButtonData data = missionbutton.GetComponent<ButtonData>();
            data.SetMission(node["yet_do"][i][1].Value, node["yet_do"][i][0].AsInt, node["yet_do"][i][3].AsInt, node["yet_do"][i][4].AsInt,false);
        }
        if (node["do"].Count + node["yet_do"].Count > 11)
        {
            //mMissionPanel.sizeDelta = new Vector2(0, (node.Count % 2 + node.Count - 11) / 2 * (75f));
        }
        else
        {
            //mMissionPanel.sizeDelta = new Vector2(0, 0);
        }
    }

    public void CloseGameCanvas()
    {
        //ChangeMode(IPlayerMode.Mission);
        GameCanvasPanel.SetActive(false);
    }

    public void OpenGameCanvas()
    {
        GameCanvasPanel.SetActive(true);
    }

    IEnumerator LoadClassics()
    {
        Debug.Log("讀取classics");
        mClassicComplete = true;
        yield return null;
        //var form = new WWWForm();
        //JSONClass JSon = new JSONClass();
        //JSon.Add("toDo", "loadCards");
        //JSon.Add("UserID", GameManage.Instance.ThePlayerData.PlayerID.ToString());
        //form.AddField("response", JSon.ToString());
        //var sendWWW = new WWW(MyVar.PHPURL, form);
        //yield return sendWWW;
        //if (sendWWW.error != null)
        //{
        //    print("error: " + sendWWW.error);
        //}
        //else
        //{
        //    mCardResult = JSONNode.Parse(sendWWW.text);
        //    Debug.LogError(mCardResult["output"]);
        //    if (mCardResult["output"].Count > 0)
        //    {
        //        //for (int i = 0; i < mCardResult["output"].Count; i++)
        //        //{
        //        //    Instantiate(ObjectPool.Instance.LoadObject("CardImage"), mCollectionContent.transform);
        //        //}
        //        //if (mCardResult["output"].Count > 9)
        //        //{
        //        //    mCollectionContent.sizeDelta = new Vector2(0,( mCardResult["output"].Count % 3 +(mCardResult["output"].Count - 9) / 3 )* (250f));
        //        //}
        //        //else
        //        //{
        //        //    mCollectionContent.sizeDelta = new Vector2(0, 0);
        //        //}
        //    }
        //    else
        //    {
        //        mClassicComplete = true;
        //    }
        //}
    }

    IEnumerator loadMissionList()
    {
        Debug.Log("讀取任務list");
        yield return true;
        //var form = new WWWForm();
        //JSONClass JSon = new JSONClass();
        //JSon.Add("toDo", "loadMissionList");
        //JSon.Add("UserID", GameManage.Instance.ThePlayerData.PlayerID.ToString());
        //form.AddField("response", JSon.ToString());
        //var sendWWW = new WWW(MyVar.PHPURL, form);
        //yield return sendWWW;
        //if (sendWWW.error != null)
        //{
        //    //GameObject.Find ("Warning").GetComponent<Text>().text="error: "+sendWWW.error;
        //    print("error: " + sendWWW.error);
        //}
        //else
        //{
        //    print(sendWWW.text);
        //    var Result = JSONNode.Parse(sendWWW.text);

        //    LoadMissionFromCache(Result);
        //}
    }

    IEnumerator loadRanking()
    {
        Debug.Log("讀取ranking");
        MRankingComplete = true;
        yield return null;
        //var form = new WWWForm();
        //JSONClass JSon = new JSONClass();
        //JSon.Add("toDo", "loadRanking");
        //JSon.Add("UserID", GameManage.Instance.ThePlayerData.PlayerID.ToString());


        //JSon.Add("schoolID", "1");
        //form.AddField("response", JSon.ToString());
        //var sendWWW = new WWW(MyVar.PHPURL, form);
        //yield return sendWWW;
        //if (sendWWW.error != null)
        //{
        //    print("error: " + sendWWW.error);
        //}
        //else
        //{
        //    print(sendWWW.text);
        //    var Result = JSONNode.Parse(sendWWW.text);
        //    mRankResult = Result;
        //    for (int i = 0; i < Result["coinList"].Count; i++)
        //    {
        //        Instantiate(ObjectPool.Instance.LoadObject("RankPanel"), mRankContent.transform);
        //    }

        //    if (Result["coinList"].Count > 12)
        //    {
        //        mRankContent.sizeDelta = new Vector2(0, (Result["coinList"].Count % 2 + Result["coinList"].Count - 12) * (75f));
        //    }
        //    else
        //    {
        //        mRankContent.sizeDelta = new Vector2(0, 0);
        //    }

        //    for (int i = 0; i < Result["coinList"].Count; i++)
        //    {
        //        if (Result["coinList"][i][0].AsInt == GameManage.Instance.ThePlayerData.PlayerID)
        //        {
        //            if (Result["coinList"][i][3].AsInt > 0)
        //            {
        //                LoadRank loadrank = mPlayerRankPanel.GetComponent<LoadRank>();
        //                loadrank.SetRankMessage(Result["coinList"][i][1], Result["coinList"][i][3].AsInt);
        //            }
        //            else
        //            {
        //                //youScore.transform.Find("order").GetComponent<Text>().text = "-";
        //                Debug.Log("no Score");
        //                LoadRank loadrank = mPlayerRankPanel.GetComponent<LoadRank>();
        //                loadrank.SetRankMessage(Result["coinList"][i][1], 0);
        //            }
        //        }
        //    }
        //    MRankingComplete = true;
        //}
    }

    IEnumerator LoadTreasure()
    {
        Debug.Log("讀取Treasure");
        mTreasureComplete = true;
        yield return null;
    }

    IEnumerator CheckLoading()
    {
        if (!(MRankingComplete && mClassicComplete && mTreasureComplete))
        {
            Debug.Log("mRankingComplete : " + MRankingComplete + "CollectionComplete : " + mClassicComplete);
            Debug.Log("CollectionComplete : " + mClassicComplete);
            Debug.Log("mTreasureComplete : " + mTreasureComplete);
            yield return new WaitForSeconds(0.25f);
            yield return new WaitForSeconds(0.25f);
            yield return StartCoroutine(CheckLoading());
        }
        else
        {
            GameManage.Instance.LoadComplete();
        }
    }
}
