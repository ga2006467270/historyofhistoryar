﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadRank : MonoBehaviour
{
    [SerializeField] Text mNameText;
    [SerializeField] Text mMoneyText;
    [SerializeField] Image mRankImage;
    [SerializeField] Sprite[] mRankLevelImg;
    int rank;
    // 有ID值 但目前無作用
    //[SerializeField] Text mIdText;
    JSONNode mNode;
    [SerializeField] bool mLoadOnAwake = true;
    private void Awake()
    {
        if(mLoadOnAwake)
        {
            SetRankMessage();
        }
    }

    void SetRankMessage()
    {
        mNode = PlayerMode.Instance.RankValue;
        rank = PlayerMode.Instance.RankCount;
        switch (rank)
        {
            case 1:
                mRankImage.sprite = mRankLevelImg[0];
                break;
            case 2:
                mRankImage.sprite = mRankLevelImg[1];
                break;
            default:
                mRankImage.sprite = mRankLevelImg[2];
                break;
        }
        mNameText.text = mNode[1];
        mMoneyText.text = mNode[3];
    }

    public void SetRankMessage(string name,int money)
    {
        mNameText.text = name;
        mMoneyText.text = money.ToString();
    }
}
