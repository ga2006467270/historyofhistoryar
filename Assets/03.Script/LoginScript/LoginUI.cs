﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;

public class LoginUI : MonoBehaviour
{
    [Header("AccountUI")]
    [SerializeField] InputField mAccount;
    [SerializeField] InputField mPassword;
    [SerializeField] Image KeepLoginImage;
    [SerializeField] Text mWrongIDText;

    [Space]

    [Header("LogoUI")]
    [SerializeField] Image mBigDipperLogo;
    [SerializeField] float mShowLogoSpeed;
    [SerializeField] float mShowLogoTime;
    [SerializeField] float mFadeLogoSpeed;

    [Space]

    [SerializeField] GameObject mLoginPanel;

    private static LoginUI mInstance;
    bool mLoadLoginData = false;

    public static LoginUI Instance
    {
        get
        {
            if (mInstance == null)
            {
                Debug.LogError("無此Instance");
            }
            return mInstance;
        }
    }

    private void Awake()
    {
        if (mInstance != null)
        {
            Debug.LogError("兩個以上的Instance");
            Destroy(gameObject);
        }
        mInstance = this;
    }

    public void CheckPreData()
    {
        GameManage.Instance.ThePlayerData = CheckAccountPrefab();
        if (GameManage.Instance.ThePlayerData != null)
        {
            mAccount.text = GameManage.Instance.ThePlayerData.SaveAccount;
            mPassword.text = GameManage.Instance.ThePlayerData.SavePassword;
            if (KeepLoginImage.color.a == 0)
            {
                SetKeepLogin();
            }
        }
    }

    PlayerData CheckAccountPrefab()
    {
        string path = Application.persistentDataPath + "/playerPassPrefab";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream stream = new FileStream(path, FileMode.Open))
            {
                PlayerData data = formatter.Deserialize(stream) as PlayerData;
                return data;
            }
        }
        return null;
    }

    public void GuestAccount() 
    {
        Debug.Log("訪客登入");
    }

    public void AccountCheck()
    {
        if (mAccount.text == "")
        {
            Debug.Log("空的帳號");
            return;
        }
        else if (mPassword.text == "")
        {
            Debug.Log("空的密碼");
            return;
        }
        else if (mLoadLoginData)
        {
            Debug.Log("資料正在讀取中...");
            return;
        }
        else
        {
            mLoadLoginData = true;
            StartCoroutine(LoginTest());
            Debug.Log("進入遊戲");
        }
        if (KeepLoginImage.IsActive())
        {
            Debug.Log("記住帳號/密碼");
            SaveAccountPrefab();
        }
        // 進入遊戲Func
    }

    void SaveAccountPrefab()
    {
        string path = Application.persistentDataPath + "/playerPassPrefab";
        using (FileStream stream = new FileStream(path, FileMode.Create))
        {
            BinaryFormatter formatter = new BinaryFormatter();

            PlayerData data = new PlayerData();
            data.SetPlayerData(mAccount.text, mPassword.text);
            formatter.Serialize(stream, data);
        }
    }
    IEnumerator LoginTest()
    {
        Debug.Log("登入帳號");
        Debug.Log("帳號正確");
        Debug.Log("進入場景");
        mLoadLoginData = true;
        SetLoginUI(false);
        PlayerMode.Instance.LoadData(1447); //待修正
        mLoadLoginData = false;
        Debug.LogError("mLoadLoginData待修正");
        yield return null;
    }

    IEnumerator Login()
    {
        var form = new WWWForm();
        JSONClass JSon = new JSONClass();
        JSon.Add("toDo", "login");
        JSon.Add("Username", mAccount.text);
        JSon.Add("password", mPassword.text);
        JSon.Add("version", MyVar.version);
        string Platform = "0";
#if UNITY_IPHONE
		Platform= "0";
#elif UNITY_ANDROID
        Platform = "1";
#elif UNITY_EDITOR
        Platform = "2";
#endif
        JSon.Add("Platform", Platform);
        //string version = myVar.version;
        //JSon.Add ("version",version);
        Debug.Log(Platform);

        form.AddField("response", JSon.ToString());
        var LoginWWW = new WWW(MyVar.PHPURL, form);
        yield return LoginWWW;
        //print (LoginWWW.text);
        if (LoginWWW.error != null)
        {
            //GameObject.Find("Warning").GetComponent<Text>().text = "error: " + LoginWWW.error;
            Debug.LogError("error: " + LoginWWW.error);
        }
        else
        {
            mWrongIDText.gameObject.SetActive(false);
            //print ("LOGIN: "+LoginWWW.text);
            var Result = JSONNode.Parse(LoginWWW.text);
            Debug.Log(Result);
            if (Result["result"].Value.ToString().CompareTo("success") == 0)
            {
                GameManage.Instance.UseLoadingBar();
                Debug.Log(Result["data"][0][0].AsInt);
                Debug.Log(Result["result"]);
                Debug.Log("Your Coins is : " + Result["data"][0][2].AsInt);
                Debug.Log("Your Mission State is : " + Result["data"][0][3].AsInt);
                GameManage.Instance.SavePlayerData(Result);
                GameManage.Instance.ChangeToGame();
                PlayerMode.Instance.LoadData(Result["data"][0][0].AsInt);
                SetLoginUI(false);
            }
            else
            {
                mWrongIDText.gameObject.SetActive(true);
                mWrongIDText.text = Result["result"].Value.ToString();
                yield return null;
            }
            mLoadLoginData = false;
        }
    }

    public void SetLoginUI(bool activemode)
    {
        mLoginPanel.SetActive(activemode);
    }

    public IEnumerator ShowLogo()
    {
        while (mBigDipperLogo.color.a <= 1)
        {
            mBigDipperLogo.color = new Color(mBigDipperLogo.color.r, mBigDipperLogo.color.g, mBigDipperLogo.color.b, mBigDipperLogo.color.a + mShowLogoSpeed * Time.deltaTime);
            yield return new WaitForFixedUpdate();
        }
        yield return new WaitForSeconds(mShowLogoTime);
        yield return StartCoroutine(FadeLogo());
    }

    IEnumerator FadeLogo()
    {
        while (mBigDipperLogo.color.a >= 0)
        {
            mBigDipperLogo.color = new Color(mBigDipperLogo.color.r, mBigDipperLogo.color.g, mBigDipperLogo.color.b, mBigDipperLogo.color.a - mFadeLogoSpeed * Time.deltaTime);
            yield return new WaitForFixedUpdate();
        }
        Destroy(mBigDipperLogo.gameObject);
        yield return null;
    }

    public void SetKeepLogin()
    {

        if (KeepLoginImage.color.a == 0)
        {
            KeepLoginImage.color = new Color(KeepLoginImage.color.r, KeepLoginImage.color.g, KeepLoginImage.color.b, 1);
        }
        else
        {
            KeepLoginImage.color = new Color(KeepLoginImage.color.r, KeepLoginImage.color.g, KeepLoginImage.color.b, 0);
        }
    }
}
