﻿using UnityEngine;

/// <summary>
/// Resource工廠
/// </summary>
public class ResourceLoad : MonoBehaviour
{
    public enum eSystemType
    {
        HistoryOfHistory
    }

    public static ResourceLoad Instance
    {
        get
        {
            if (mInstance == null)
            {
                var gamemanager = new GameObject("ResourceLoad");
                gamemanager.AddComponent<ResourceLoad>();
            }
            return mInstance;
        }
    }

    [SerializeField] eSystemType mSystemType;
    static ResourceLoad mInstance;
    RsourceStores mFactoryStore;

    public eSystemType GetSystemType()
    {
        return mSystemType;
    }

    void Awake()
    {
        mInstance = this;
    }

    public void StartResourcesLoad()
    {
        mFactoryStore = new RsourceStores(new SimpleResourceFactory());
        mFactoryStore.ResourceOrder(mSystemType);
    }

}

public class RsourceStores
{
    SimpleResourceFactory mFactory;

    public RsourceStores(SimpleResourceFactory factory)
    {
        mFactory = factory;
    }

    public void ResourceOrder(ResourceLoad.eSystemType resourceEnum)
    {
        IResourseLoadType resourcetype;
        resourcetype = mFactory.CreateResource(resourceEnum);
        resourcetype.LoadResourses();
    }
}

public class SimpleResourceFactory
{
    public IResourseLoadType CreateResource(ResourceLoad.eSystemType resourceEnum)
    {
        switch (resourceEnum)
        {
            case ResourceLoad.eSystemType.HistoryOfHistory:
                return new ResourceLoadHistoryOfHistory();
        }
        return null;
    }
}
