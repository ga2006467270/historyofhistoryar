﻿using UnityEngine;

public interface IObjectLoadType
{
    void LoadObject();
}

public class InstanciatateObject
{
    public static void InstantiateObject(string objectName)
    {
        /// <summary>
        /// 若場景上無此名子的物件 則創造一個物件
        /// </summary>
        if (ObjectPool.Instance.GetObjectInGame(objectName) == null)
        {
            GameObject poolObject = ObjectPool.Instance.LoadObject(objectName);
            ObjectPool.Instance.NewInstantiateObject(poolObject);
        }
        else
        {
            Debug.Log("場景中已有 : " + objectName);
            return;
        }
    }
}

public class ObjectLoad : IObjectLoadType
{
    public void LoadObject()
    {
        InstanciatateObject.InstantiateObject("GameManage");
        InstanciatateObject.InstantiateObject("LoadingBar");
        InstanciatateObject.InstantiateObject("LoginCanvas");
        //InstanciatateObject.InstantiateObject("LoadingCanvas");
        //InstanciatateObject.InstantiateObject("MissionManager");
    }
}