﻿using System;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;
using UnityEngine.UI;

public class GameManage : MonoBehaviour
{
    private static GameManage mInsatnce;
    public bool TestMode;

    public static GameManage Instance
    {
        get
        {
            if (mInsatnce == null)
            {
                Debug.LogError("無Instance");
            }
            return mInsatnce;
        }
    }

    public int UserID;
    public PlayerData ThePlayerData;

    // Use this for initialization
    void Awake()
    {
        if (mInsatnce != null)
        {
            Debug.LogError("有兩個以上Insance");
            Destroy(gameObject);
        }
        else
        {
            mInsatnce = this;
        }
        //ObjectPool.Instance.
    }

    private void Start()
    {
        ResourceLoad.Instance.StartResourcesLoad();
        ObjectPool.Instance.SetObjectPool();
        ObjectPool.Instance.LoadObjectPool();
        //StartCoroutine(StartGame());
        //ObjectPool.Instance.GetObjectInGame("LoadingBar").SetActive(false);
        LoginUI.Instance.CheckPreData();
    }

    public IEnumerator StartGame()
    {
        yield return StartCoroutine(LoginUI.Instance.ShowLogo());
        Debug.Log("轉換至Login");
    }

    // 讀取排行榜
    // 讀取帳號的Collection
    // 開啟MissionPanel
    public void ChangeToGame()
    {
        PlayerMode.Instance.GameCanvasPanel.SetActive(true);
    }

    public void SavePlayerData(JSONNode result)
    {
        ThePlayerData = new PlayerData();
        ThePlayerData.PlayerID = result["data"][0][0].AsInt;
        ThePlayerData.Coins = result["data"][0][2].AsInt;
        ThePlayerData.Name = result["data"][0][1].Value;
        PlayerMode.Instance.CheckPlayerCoins();
        PlayerMode.Instance.CheckPlayerName();
    }

    public void UseLoadingBar()
    {
        ObjectPool.Instance.GetObjectInGame("LoadingBar").SetActive(true);
        Debug.Log("loadingbar");
    }

    public void LoadComplete()
    {
        ObjectPool.Instance.GetObjectInGame("LoadingBar").SetActive(false);
    }
}
