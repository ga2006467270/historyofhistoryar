﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerData
{
    public string SaveAccount;
    public string SavePassword;
    public int PlayerID;
    public int Coins;
    public string Name;

    public PlayerData SetPlayerData(string account, string password)
    {
        SaveAccount = account;
        SavePassword = password;
        return this;
    }

    public void Clear()
    {
        SaveAccount = null;
        SavePassword = null;
        PlayerID = 0;
        Coins = 0;
        Name = null;
    }
}
