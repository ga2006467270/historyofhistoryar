﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TreasuerData : MonoBehaviour
{
    Treasure mTreasure = null;
    public void OpenTreasure()
    {
        if (mTreasure != null)
        {
            PlayerMode.Instance.OpenTreasureBox(mTreasure.TImg, mTreasure.TName);
        }
        else
        {
            Debug.Log("沒有寶物資料");
        }
    }

    public void SetTreasue(Sprite img,string name)
    {
        mTreasure.TImg = img;
        mTreasure.TName = name;
    }
}

public class Treasure
{
    public Sprite TImg;
    public string TName;
}
